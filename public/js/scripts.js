/* Aqui é onde a magia acontece */
/* Estou criando uma constante com a url da api para listar as categorias da navegação */
const url ="http://localhost:8888/api/V1/categories/list"

/* Realizo a chamada para a api com um fetch */
fetch(url)
/* Armazeno a resposta no objeto json e então realizo a função */
.then(response => response.json())
.then(json =>{
    /* Para navegar pelo conteudo da resposta e criar um elemento "li" e dentro dele um 'a' para cada item adquirido da api utilizei um for */
    for(let i = 0; i < json.items.length; i++){
        /* Aqui declaro algumas variaveis que vão ser utilizadas no futuro */
        var TextoJson =  json.items[i].name;
        var novoElem  = document.createElement('li');
        var texto     = document.createTextNode(TextoJson);

    /* Aqui eu começo a criar os elementos individualmente para cada item */
    var links = [];
    links[i] = document.createElement('a');
    var link = '#';
    
    /* Aqui eu anexo com um appendChild os atributos e o valor de nome na tag 'a' */
    links[i].href = link;
    links[i].appendChild(texto)
    novoElem.appendChild(links[i])

    /* Aqui eu crio as variaveis de 'ul' e 'li' e então insiro na 7ª li, pois como já tenho outros elementos de li no html para os elementos serem inseridos no lugar correto precisei especificar onde */
    var lista = document.getElementsByTagName('ul')[0];
    var itens = document.getElementsByTagName('li');
    lista.insertBefore(novoElem, itens[6]);
    }

    /* Aqui eu adiciono o atributo de onClick com as funções para cada link */
    document.getElementsByTagName('a')[8].onclick = function() { shoes(); };
    document.getElementsByTagName('a')[9].onclick = function() { pants(); };
    document.getElementsByTagName('a')[10].onclick = function() { shirt(); };
    document.getElementsByTagName('a')[11].onclick = function() { home(); };

})

/* Essa função serve para fazer o chamado para api, eu resolvi fazer com XMLHttpRequest pois assim o comportamento padrão da pagina de recarregar não aconteceria */
function fazGet(link) {
    let request = new XMLHttpRequest()
    request.open("GET", link, false)
    request.send()
    return request.responseText
}

/* Essa função cria os cards de produtos, ela é chamada em outras funções que contem os respectivos produtos, fazer isso com esse método não é a melhor forma que eu conheço, mas como não consegui utilizar react para a criação de classes e construtores onde eu conseguiria armazenar states acabei criando os elementos dessa forma*/
function criaElemento(prod) {
    /* Aqui eu crio os elementos html */ 
    div = document.createElement("div")
    picture = document.createElement("img")
    titulo = document.createElement("h3")
    preco = document.createElement("p")
    btn = document.createElement("button")

    /* Aqui eu adiciono os dados em cada respectivo atributo */ 
    titulo.innerHTML = prod.name
    picture.src = "./" + prod.image
    picture.id += "fotos"
    preco.innerHTML = "R$" + prod.price
    btn.innerHTML = "Comprar"

    /* Anexo ao elemento div */ 
    div.appendChild(picture)
    div.appendChild(titulo)
    div.appendChild(preco)
    div.appendChild(btn)

    /* Aqui eu atribuo uma classe e um ID */ 
    div.className += "card flex flex-collumn ai-center"
    div.id += "prod"

    return div
}  

/* Aqui é onde eu chamo a função do GET e a de criar elemento, fazendo a união das duas e utilizando um forEach para que seja realizada a criação de cada elemento em sequencia e atribuida seus respectivos dados */ 
function shirt() {
    let camisa = "http://localhost:8888/api/V1/categories/1"
    
    let data = fazGet(camisa)
    var produtos = JSON.parse(data)
    let card = document.getElementById("card")
    produtos.items.forEach(element => {
        let div = criaElemento(element);
        card.appendChild(div)
    });
    /* Aqui eu trabalho na estilização desses elementos que foram criados e altero os elementos que ja existem */ 
    card.style.display = "grid"

    /* Aqui eu retiro da tela atraves da manipulação da DOM os elementos da pagina inicial */ 
    document.getElementById("text").style.display = "none"
    document.getElementById("banner").style.display = "none"

    /* Como a requisição não tem limite para evitar de invocar mais de uma vez a ação de puxar os produtos eu bloqueio o acesso ao mesmo link 2 vezes, como tenho uma função para cada categoria eu apenas fui movendo a disposição dos botões nas funções seguintes */ 
    document.getElementById("camisa").style.pointerEvents = "none"
    document.getElementById("calcas").style.pointerEvents = "auto"
    document.getElementById("sapatinho").style.pointerEvents = "auto"

    document.getElementsByTagName('a')[10].pointerEvents = "none"

    /* Aqui é onde eu organizo atraves do display grid as medias queries dos elementos que criei atraves da DOM, deixando assim os cards responsivos */ 
    function screen(s) {
        if (s.matches) { 
            document.getElementById("card").style.gridTemplateColumns = "1fr 1fr"
            document.getElementById("card").style.gridTemplateRows = "auto"
        } else {
            document.getElementById("card").style.gridTemplateColumns = "1fr 1fr 1fr"
        }
      }
    var s = window.matchMedia("(max-width: 1000px)")
    screen(s)
    
    s.addListener(screen)

    function mobile(y) {
        if (y.matches) { 
            document.getElementById("card").style.gridTemplateColumns = "1fr"
            document.getElementById("card").style.gridTemplateRows = "auto"
        } else {
            document.getElementById("card").style.gridTemplateColumns = "1fr 1fr 1fr"
        }
      }
    var y = window.matchMedia("(max-width: 740px)")

    mobile(y)
    
    y.addListener(mobile)
}

/* Essa função faz as mesmas coisas que a anterior com algumas alterações, mas estruturalmente são identicas */ 
function pants() {
    let calca = "http://localhost:8888/api/V1/categories/2"
    
    let data = fazGet(calca)
    var produtos = JSON.parse(data)
    let card = document.getElementById("card")
    produtos.items.forEach(element => {
        let div = criaElemento(element);
        card.appendChild(div)
    });  
    card.style.display = "grid"
    card.style.gridTemplateColumns = "1fr 1fr 1fr"

    document.getElementById("text").style.display = "none"
    document.getElementById("banner").style.display = "none"

    document.getElementById("camisa").style.pointerEvents = "auto"
    document.getElementById("calcas").style.pointerEvents = "none"
    document.getElementById("sapatinho").style.pointerEvents = "auto"

    document.getElementsByTagName('a')[9].style.pointerEvents = "none"

    function screen(s) {
        if (s.matches) { 
            document.getElementById("card").style.gridTemplateColumns = "1fr 1fr"
            document.getElementById("card").style.gridTemplateRows = "auto"
        } else {
            document.getElementById("card").style.gridTemplateColumns = "1fr 1fr 1fr"
        }
      }
    var s = window.matchMedia("(max-width: 1000px)")
    screen(s)
    
    s.addListener(screen)

    function mobile(y) {
        if (y.matches) { 
            document.getElementById("card").style.gridTemplateColumns = "1fr"
            document.getElementById("card").style.gridTemplateRows = "auto"
        } else {
            document.getElementById("card").style.gridTemplateColumns = "1fr 1fr 1fr"
        }
      }
    var y = window.matchMedia("(max-width: 740px)")

    mobile(y)
    
    y.addListener(mobile)
}

/* Essa aqui tambem é igual a anterior */ 
async function shoes() {
    let sapato = "http://localhost:8888/api/V1/categories/3"
    
    let data = fazGet(sapato)
    var produtos = JSON.parse(data)
    let card = document.getElementById("card")
    produtos.items.forEach(element => {
        let div = criaElemento(element);
        card.appendChild(div)
    });   
    card.style.display = "grid"
    card.style.gridTemplateColumns = "1fr 1fr 1fr"
    
    document.getElementById("text").style.display = "none"
    document.getElementById("banner").style.display = "none"
    
    document.getElementById("camisa").style.pointerEvents = "auto"
    document.getElementById("calcas").style.pointerEvents = "auto"
    document.getElementById("sapatinho").style.pointerEvents = "none"

    document.getElementsByTagName('a')[8].style.pointerEvents = "none"

    function screen(s) {
        if (s.matches) { 
            document.getElementById("card").style.gridTemplateColumns = "1fr 1fr"
            document.getElementById("card").style.gridTemplateRows = "auto"
        } else {
            document.getElementById("card").style.gridTemplateColumns = "1fr 1fr 1fr"
        }
      }
    var s = window.matchMedia("(max-width: 1000px)")
    screen(s)
    
    s.addListener(screen)

    function mobile(y) {
        if (y.matches) { 
            document.getElementById("card").style.gridTemplateColumns = "1fr"
            document.getElementById("card").style.gridTemplateRows = "auto"
        } else {
            document.getElementById("card").style.gridTemplateColumns = "1fr 1fr 1fr"
        }
      }
    var y = window.matchMedia("(max-width: 740px)")

    mobile(y)
    
    y.addListener(mobile)
}

/* Essa ultima função é a que recarrega a pagina levando a pessoa de volta para a tela inicial, como os produtos são apenas gerados na DOM é mais conveniente recarregar a página para voltar ao inicio do que apagar todos os elementos gerados. */ 
function home() {
    window.location.reload(false)
}

